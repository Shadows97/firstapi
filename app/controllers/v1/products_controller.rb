class V1::ProductsController < ApplicationController

  skip_before_action :verify_authenticity_token

  def index
    render json: Product.all , status: :ok
  end

  def show
    @product = Product.find(params[:id])
    @request = Faraday.get('https://restcountries.eu/rest/v2/all') do |req|
    end
    render json: @product, status: :ok
  end

  def create
    @products = Product.new(product_param)
    @products.save
    render json: @products, status: :created
  end

  def update
    @product = Product.find(params[:id])
    @product = @product.update(product_param)
    render json: @product, status: :created
  end

  def destroy
    @product = Product.where(id: params[:id]).first
    if @product.destroy
      head(:ok)
    else
      head(:unprocessable_entity)
    end
  end

  private
  def product_param
    params.require(:product).permit(:name,:description,:price,:quantity)
  end
end
